export const GEN_II_POKEMONS = [
    {
        "name": "Chikorita",
        "id": 152
    },
    {
        "name": "Bayleef",
        "id": 153
    },
    {
        "name": "Meganium",
        "id": 154
    },
    {
        "name": "Cyndaquil",
        "id": 155
    },
    {
        "name": "Quilava",
        "id": 156
    },
    {
        "name": "Typhlosion",
        "id": 157
    },
    {
        "name": "Totodile",
        "id": 158
    },
    {
        "name": "Croconaw",
        "id": 159
    },
    {
        "name": "Feraligatr",
        "id": 160
    },
    {
        "name": "Sentret",
        "id": 161
    },
    {
        "name": "Furret",
        "id": 162
    },
    {
        "name": "Hoothoot",
        "id": 163
    },
    {
        "name": "Noctowl",
        "id": 164
    },
    {
        "name": "Ledyba",
        "id": 165
    },
    {
        "name": "Ledian",
        "id": 166
    },
    {
        "name": "Spinarak",
        "id": 167
    },
    {
        "name": "Ariados",
        "id": 168
    },
    {
        "name": "Crobat",
        "id": 169
    },
    {
        "name": "Chinchou",
        "id": 170
    },
    {
        "name": "Lanturn",
        "id": 171
    },
    {
        "name": "Pichu",
        "id": 172
    },
    {
        "name": "Cleffa",
        "id": 173
    },
    {
        "name": "Igglybuff",
        "id": 174
    },
    {
        "name": "Togepi",
        "id": 175
    },
    {
        "name": "Togetic",
        "id": 176
    },
    {
        "name": "Natu",
        "id": 177
    },
    {
        "name": "Xatu",
        "id": 178
    },
    {
        "name": "Mareep",
        "id": 179
    },
    {
        "name": "Flaaffy",
        "id": 180
    },
    {
        "name": "Ampharos",
        "id": 181
    },
    {
        "name": "Bellossom",
        "id": 182
    },
    {
        "name": "Marill",
        "id": 183
    },
    {
        "name": "Azumarill",
        "id": 184
    },
    {
        "name": "Sudowoodo",
        "id": 185
    },
    {
        "name": "Politoed",
        "id": 186
    },
    {
        "name": "Hoppip",
        "id": 187
    },
    {
        "name": "Skiploom",
        "id": 188
    },
    {
        "name": "Jumpluff",
        "id": 189
    },
    {
        "name": "Aipom",
        "id": 190
    },
    {
        "name": "Sunkern",
        "id": 191
    },
    {
        "name": "Sunflora",
        "id": 192
    },
    {
        "name": "Yanma",
        "id": 193
    },
    {
        "name": "Wooper",
        "id": 194
    },
    {
        "name": "Quagsire",
        "id": 195
    },
    {
        "name": "Espeon",
        "id": 196
    },
    {
        "name": "Umbreon",
        "id": 197
    },
    {
        "name": "Murkrow",
        "id": 198
    },
    {
        "name": "Slowking",
        "id": 199
    },
    {
        "name": "Misdreavus",
        "id": 200
    },
    {
        "name": "Unown",
        "id": 201
    },
    {
        "name": "Wobbuffet",
        "id": 202
    },
    {
        "name": "Girafarig",
        "id": 203
    },
    {
        "name": "Pineco",
        "id": 204
    },
    {
        "name": "Forretress",
        "id": 205
    },
    {
        "name": "Dunsparce",
        "id": 206
    },
    {
        "name": "Gligar",
        "id": 207
    },
    {
        "name": "Steelix",
        "id": 208
    },
    {
        "name": "Snubbull",
        "id": 209
    },
    {
        "name": "Granbull",
        "id": 210
    },
    {
        "name": "Qwilfish",
        "id": 211
    },
    {
        "name": "Scizor",
        "id": 212
    },
    {
        "name": "Shuckle",
        "id": 213
    },
    {
        "name": "Heracross",
        "id": 214
    },
    {
        "name": "Sneasel",
        "id": 215
    },
    {
        "name": "Teddiursa",
        "id": 216
    },
    {
        "name": "Ursaring",
        "id": 217
    },
    {
        "name": "Slugma",
        "id": 218
    },
    {
        "name": "Magcargo",
        "id": 219
    },
    {
        "name": "Swinub",
        "id": 220
    },
    {
        "name": "Piloswine",
        "id": 221
    },
    {
        "name": "Corsola",
        "id": 222
    },
    {
        "name": "Remoraid",
        "id": 223
    },
    {
        "name": "Octillery",
        "id": 224
    },
    {
        "name": "Delibird",
        "id": 225
    },
    {
        "name": "Mantine",
        "id": 226
    },
    {
        "name": "Skarmory",
        "id": 227
    },
    {
        "name": "Houndour",
        "id": 228
    },
    {
        "name": "Houndoom",
        "id": 229
    },
    {
        "name": "Kingdra",
        "id": 230
    },
    {
        "name": "Phanpy",
        "id": 231
    },
    {
        "name": "Donphan",
        "id": 232
    },
    {
        "name": "Porygon2",
        "id": 233
    },
    {
        "name": "Stantler",
        "id": 234
    },
    {
        "name": "Smeargle",
        "id": 235
    },
    {
        "name": "Tyrogue",
        "id": 236
    },
    {
        "name": "Hitmontop",
        "id": 237
    },
    {
        "name": "Smoochum",
        "id": 238
    },
    {
        "name": "Elekid",
        "id": 239
    },
    {
        "name": "Magby",
        "id": 240
    },
    {
        "name": "Miltank",
        "id": 241
    },
    {
        "name": "Blissey",
        "id": 242
    },
    {
        "name": "Raikou",
        "id": 243
    },
    {
        "name": "Entei",
        "id": 244
    },
    {
        "name": "Suicune",
        "id": 245
    },
    {
        "name": "Larvitar",
        "id": 246
    },
    {
        "name": "Pupitar",
        "id": 247
    },
    {
        "name": "Tyranitar",
        "id": 248
    },
    {
        "name": "Lugia",
        "id": 249
    },
    {
        "name": "Ho-oh",
        "id": 250
    },
    {
        "name": "Celebi",
        "id": 251
    }
]