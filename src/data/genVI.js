export const GEN_VI_POKEMONS = [
    {
        "name": "Chespin",
        "id": 650
    },
    {
        "name": "Quilladin",
        "id": 651
    },
    {
        "name": "Chesnaught",
        "id": 652
    },
    {
        "name": "Fennekin",
        "id": 653
    },
    {
        "name": "Braixen",
        "id": 654
    },
    {
        "name": "Delphox",
        "id": 655
    },
    {
        "name": "Froakie",
        "id": 656
    },
    {
        "name": "Frogadier",
        "id": 657
    },
    {
        "name": "Greninja",
        "id": 658
    },
    {
        "name": "Bunnelby",
        "id": 659
    },
    {
        "name": "Diggersby",
        "id": 660
    },
    {
        "name": "Fletchling",
        "id": 661
    },
    {
        "name": "Fletchinder",
        "id": 662
    },
    {
        "name": "Talonflame",
        "id": 663
    },
    {
        "name": "Scatterbug",
        "id": 664
    },
    {
        "name": "Spewpa",
        "id": 665
    },
    {
        "name": "Vivillon",
        "id": 666
    },
    {
        "name": "Litleo",
        "id": 667
    },
    {
        "name": "Pyroar",
        "id": 668
    },
    {
        "name": "Flabébé",
        "id": 669
    },
    {
        "name": "Floette",
        "id": 670
    },
    {
        "name": "Florges",
        "id": 671
    },
    {
        "name": "Skiddo",
        "id": 672
    },
    {
        "name": "Gogoat",
        "id": 673
    },
    {
        "name": "Pancham",
        "id": 674
    },
    {
        "name": "Pangoro",
        "id": 675
    },
    {
        "name": "Furfrou",
        "id": 676
    },
    {
        "name": "Espurr",
        "id": 677
    },
    {
        "name": "Meowstic",
        "id": 678
    },
    {
        "name": "Honedge",
        "id": 679
    },
    {
        "name": "Doublade",
        "id": 680
    },
    {
        "name": "Aegislash",
        "id": 681
    },
    {
        "name": "Spritzee",
        "id": 682
    },
    {
        "name": "Aromatisse",
        "id": 683
    },
    {
        "name": "Swirlix",
        "id": 684
    },
    {
        "name": "Slurpuff",
        "id": 685
    },
    {
        "name": "Inkay",
        "id": 686
    },
    {
        "name": "Malamar",
        "id": 687
    },
    {
        "name": "Binacle",
        "id": 688
    },
    {
        "name": "Barbaracle",
        "id": 689
    },
    {
        "name": "Skrelp",
        "id": 690
    },
    {
        "name": "Dragalge",
        "id": 691
    },
    {
        "name": "Clauncher",
        "id": 692
    },
    {
        "name": "Clawitzer",
        "id": 693
    },
    {
        "name": "Helioptile",
        "id": 694
    },
    {
        "name": "Heliolisk",
        "id": 695
    },
    {
        "name": "Tyrunt",
        "id": 696
    },
    {
        "name": "Tyrantrum",
        "id": 697
    },
    {
        "name": "Amaura",
        "id": 698
    },
    {
        "name": "Aurorus",
        "id": 699
    },
    {
        "name": "Sylveon",
        "id": 700
    },
    {
        "name": "Hawlucha",
        "id": 701
    },
    {
        "name": "Dedenne",
        "id": 702
    },
    {
        "name": "Carbink",
        "id": 703
    },
    {
        "name": "Goomy",
        "id": 704
    },
    {
        "name": "Sliggoo",
        "id": 705
    },
    {
        "name": "Goodra",
        "id": 706
    },
    {
        "name": "Klefki",
        "id": 707
    },
    {
        "name": "Phantump",
        "id": 708
    },
    {
        "name": "Trevenant",
        "id": 709
    },
    {
        "name": "Pumpkaboo",
        "id": 710
    },
    {
        "name": "Gourgeist",
        "id": 711
    },
    {
        "name": "Bergmite",
        "id": 712
    },
    {
        "name": "Avalugg",
        "id": 713
    },
    {
        "name": "Noibat",
        "id": 714
    },
    {
        "name": "Noivern",
        "id": 715
    },
    {
        "name": "Xerneas",
        "id": 716
    },
    {
        "name": "Yveltal",
        "id": 717
    },
    {
        "name": "Zygarde",
        "id": 718
    },
    {
        "name": "Diancie",
        "id": 719
    },
    {
        "name": "Hoopa",
        "id": 720
    },
    {
        "name": "Volcanion",
        "id": 721
    }
]