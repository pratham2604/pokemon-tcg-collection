import { GEN_I_POKEMONS } from "./genI";
import { GEN_II_POKEMONS } from "./genII";
import { GEN_III_POKEMONS } from "./genIII";
import { GEN_IV_POKEMONS } from "./genIV";
import { GEN_V_POKEMONS } from "./genV";
import { GEN_VI_POKEMONS } from "./genVI";
import { GEN_VII_POKEMONS } from "./genVII";
import { GEN_VIII_POKEMONS } from "./genVIII";

const ALL_POKEMONS = GEN_I_POKEMONS
  .concat(GEN_II_POKEMONS)
  .concat(GEN_III_POKEMONS)
  .concat(GEN_IV_POKEMONS)
  .concat(GEN_V_POKEMONS)
  .concat(GEN_VI_POKEMONS)
  .concat(GEN_VII_POKEMONS)
  .concat(GEN_VIII_POKEMONS)

const POKEMONS = {
  gen_I: GEN_I_POKEMONS,
  gen_II: GEN_II_POKEMONS,
  gen_III: GEN_III_POKEMONS,
  gen_IV: GEN_IV_POKEMONS,
  gen_V: GEN_V_POKEMONS,
  gen_VI: GEN_VI_POKEMONS,
  gen_VII: GEN_VII_POKEMONS,
  gen_VIII: GEN_VIII_POKEMONS,
  all: ALL_POKEMONS
}

export default POKEMONS;
