export const GEN_III_POKEMONS = [
    {
        "name": "Treecko",
        "id": 252
    },
    {
        "name": "Grovyle",
        "id": 253
    },
    {
        "name": "Sceptile",
        "id": 254
    },
    {
        "name": "Torchic",
        "id": 255
    },
    {
        "name": "Combusken",
        "id": 256
    },
    {
        "name": "Blaziken",
        "id": 257
    },
    {
        "name": "Mudkip",
        "id": 258
    },
    {
        "name": "Marshtomp",
        "id": 259
    },
    {
        "name": "Swampert",
        "id": 260
    },
    {
        "name": "Poochyena",
        "id": 261
    },
    {
        "name": "Mightyena",
        "id": 262
    },
    {
        "name": "Zigzagoon",
        "id": 263
    },
    {
        "name": "Linoone",
        "id": 264
    },
    {
        "name": "Wurmple",
        "id": 265
    },
    {
        "name": "Silcoon",
        "id": 266
    },
    {
        "name": "Beautifly",
        "id": 267
    },
    {
        "name": "Cascoon",
        "id": 268
    },
    {
        "name": "Dustox",
        "id": 269
    },
    {
        "name": "Lotad",
        "id": 270
    },
    {
        "name": "Lombre",
        "id": 271
    },
    {
        "name": "Ludicolo",
        "id": 272
    },
    {
        "name": "Seedot",
        "id": 273
    },
    {
        "name": "Nuzleaf",
        "id": 274
    },
    {
        "name": "Shiftry",
        "id": 275
    },
    {
        "name": "Taillow",
        "id": 276
    },
    {
        "name": "Swellow",
        "id": 277
    },
    {
        "name": "Wingull",
        "id": 278
    },
    {
        "name": "Pelipper",
        "id": 279
    },
    {
        "name": "Ralts",
        "id": 280
    },
    {
        "name": "Kirlia",
        "id": 281
    },
    {
        "name": "Gardevoir",
        "id": 282
    },
    {
        "name": "Surskit",
        "id": 283
    },
    {
        "name": "Masquerain",
        "id": 284
    },
    {
        "name": "Shroomish",
        "id": 285
    },
    {
        "name": "Breloom",
        "id": 286
    },
    {
        "name": "Slakoth",
        "id": 287
    },
    {
        "name": "Vigoroth",
        "id": 288
    },
    {
        "name": "Slaking",
        "id": 289
    },
    {
        "name": "Nincada",
        "id": 290
    },
    {
        "name": "Ninjask",
        "id": 291
    },
    {
        "name": "Shedinja",
        "id": 292
    },
    {
        "name": "Whismur",
        "id": 293
    },
    {
        "name": "Loudred",
        "id": 294
    },
    {
        "name": "Exploud",
        "id": 295
    },
    {
        "name": "Makuhita",
        "id": 296
    },
    {
        "name": "Hariyama",
        "id": 297
    },
    {
        "name": "Azurill",
        "id": 298
    },
    {
        "name": "Nosepass",
        "id": 299
    },
    {
        "name": "Skitty",
        "id": 300
    },
    {
        "name": "Delcatty",
        "id": 301
    },
    {
        "name": "Sableye",
        "id": 302
    },
    {
        "name": "Mawile",
        "id": 303
    },
    {
        "name": "Aron",
        "id": 304
    },
    {
        "name": "Lairon",
        "id": 305
    },
    {
        "name": "Aggron",
        "id": 306
    },
    {
        "name": "Meditite",
        "id": 307
    },
    {
        "name": "Medicham",
        "id": 308
    },
    {
        "name": "Electrike",
        "id": 309
    },
    {
        "name": "Manectric",
        "id": 310
    },
    {
        "name": "Plusle",
        "id": 311
    },
    {
        "name": "Minun",
        "id": 312
    },
    {
        "name": "Volbeat",
        "id": 313
    },
    {
        "name": "Illumise",
        "id": 314
    },
    {
        "name": "Roselia",
        "id": 315
    },
    {
        "name": "Gulpin",
        "id": 316
    },
    {
        "name": "Swalot",
        "id": 317
    },
    {
        "name": "Carvanha",
        "id": 318
    },
    {
        "name": "Sharpedo",
        "id": 319
    },
    {
        "name": "Wailmer",
        "id": 320
    },
    {
        "name": "Wailord",
        "id": 321
    },
    {
        "name": "Numel",
        "id": 322
    },
    {
        "name": "Camerupt",
        "id": 323
    },
    {
        "name": "Torkoal",
        "id": 324
    },
    {
        "name": "Spoink",
        "id": 325
    },
    {
        "name": "Grumpig",
        "id": 326
    },
    {
        "name": "Spinda",
        "id": 327
    },
    {
        "name": "Trapinch",
        "id": 328
    },
    {
        "name": "Vibrava",
        "id": 329
    },
    {
        "name": "Flygon",
        "id": 330
    },
    {
        "name": "Cacnea",
        "id": 331
    },
    {
        "name": "Cacturne",
        "id": 332
    },
    {
        "name": "Swablu",
        "id": 333
    },
    {
        "name": "Altaria",
        "id": 334
    },
    {
        "name": "Zangoose",
        "id": 335
    },
    {
        "name": "Seviper",
        "id": 336
    },
    {
        "name": "Lunatone",
        "id": 337
    },
    {
        "name": "Solrock",
        "id": 338
    },
    {
        "name": "Barboach",
        "id": 339
    },
    {
        "name": "Whiscash",
        "id": 340
    },
    {
        "name": "Corphish",
        "id": 341
    },
    {
        "name": "Crawdaunt",
        "id": 342
    },
    {
        "name": "Baltoy",
        "id": 343
    },
    {
        "name": "Claydol",
        "id": 344
    },
    {
        "name": "Lileep",
        "id": 345
    },
    {
        "name": "Cradily",
        "id": 346
    },
    {
        "name": "Anorith",
        "id": 347
    },
    {
        "name": "Armaldo",
        "id": 348
    },
    {
        "name": "Feebas",
        "id": 349
    },
    {
        "name": "Milotic",
        "id": 350
    },
    {
        "name": "Castform",
        "id": 351
    },
    {
        "name": "Kecleon",
        "id": 352
    },
    {
        "name": "Shuppet",
        "id": 353
    },
    {
        "name": "Banette",
        "id": 354
    },
    {
        "name": "Duskull",
        "id": 355
    },
    {
        "name": "Dusclops",
        "id": 356
    },
    {
        "name": "Tropius",
        "id": 357
    },
    {
        "name": "Chimecho",
        "id": 358
    },
    {
        "name": "Absol",
        "id": 359
    },
    {
        "name": "Wynaut",
        "id": 360
    },
    {
        "name": "Snorunt",
        "id": 361
    },
    {
        "name": "Glalie",
        "id": 362
    },
    {
        "name": "Spheal",
        "id": 363
    },
    {
        "name": "Sealeo",
        "id": 364
    },
    {
        "name": "Walrein",
        "id": 365
    },
    {
        "name": "Clamperl",
        "id": 366
    },
    {
        "name": "Huntail",
        "id": 367
    },
    {
        "name": "Gorebyss",
        "id": 368
    },
    {
        "name": "Relicanth",
        "id": 369
    },
    {
        "name": "Luvdisc",
        "id": 370
    },
    {
        "name": "Bagon",
        "id": 371
    },
    {
        "name": "Shelgon",
        "id": 372
    },
    {
        "name": "Salamence",
        "id": 373
    },
    {
        "name": "Beldum",
        "id": 374
    },
    {
        "name": "Metang",
        "id": 375
    },
    {
        "name": "Metagross",
        "id": 376
    },
    {
        "name": "Regirock",
        "id": 377
    },
    {
        "name": "Regice",
        "id": 378
    },
    {
        "name": "Registeel",
        "id": 379
    },
    {
        "name": "Latias",
        "id": 380
    },
    {
        "name": "Latios",
        "id": 381
    },
    {
        "name": "Kyogre",
        "id": 382
    },
    {
        "name": "Groudon",
        "id": 383
    },
    {
        "name": "Rayquaza",
        "id": 384
    },
    {
        "name": "Jirachi",
        "id": 385
    },
    {
        "name": "Deoxys",
        "id": 386
    }
]