export const GEN_VII_POKEMONS = [
    {
        "name": "Rowlet",
        "id": 722
    },
    {
        "name": "Dartrix",
        "id": 723
    },
    {
        "name": "Decidueye",
        "id": 724
    },
    {
        "name": "Litten",
        "id": 725
    },
    {
        "name": "Torracat",
        "id": 726
    },
    {
        "name": "Incineroar",
        "id": 727
    },
    {
        "name": "Popplio",
        "id": 728
    },
    {
        "name": "Brionne",
        "id": 729
    },
    {
        "name": "Primarina",
        "id": 730
    },
    {
        "name": "Pikipek",
        "id": 731
    },
    {
        "name": "Trumbeak",
        "id": 732
    },
    {
        "name": "Toucannon",
        "id": 733
    },
    {
        "name": "Yungoos",
        "id": 734
    },
    {
        "name": "Gumshoos",
        "id": 735
    },
    {
        "name": "Grubbin",
        "id": 736
    },
    {
        "name": "Charjabug",
        "id": 737
    },
    {
        "name": "Vikavolt",
        "id": 738
    },
    {
        "name": "Crabrawler",
        "id": 739
    },
    {
        "name": "Crabominable",
        "id": 740
    },
    {
        "name": "Oricorio",
        "id": 741
    },
    {
        "name": "Cutiefly",
        "id": 742
    },
    {
        "name": "Ribombee",
        "id": 743
    },
    {
        "name": "Rockruff",
        "id": 744
    },
    {
        "name": "Lycanroc",
        "id": 745
    },
    {
        "name": "Wishiwashi",
        "id": 746
    },
    {
        "name": "Mareanie",
        "id": 747
    },
    {
        "name": "Toxapex",
        "id": 748
    },
    {
        "name": "Mudbray",
        "id": 749
    },
    {
        "name": "Mudsdale",
        "id": 750
    },
    {
        "name": "Dewpider",
        "id": 751
    },
    {
        "name": "Araquanid",
        "id": 752
    },
    {
        "name": "Fomantis",
        "id": 753
    },
    {
        "name": "Lurantis",
        "id": 754
    },
    {
        "name": "Morelull",
        "id": 755
    },
    {
        "name": "Shiinotic",
        "id": 756
    },
    {
        "name": "Salandit",
        "id": 757
    },
    {
        "name": "Salazzle",
        "id": 758
    },
    {
        "name": "Stufful",
        "id": 759
    },
    {
        "name": "Bewear",
        "id": 760
    },
    {
        "name": "Bounsweet",
        "id": 761
    },
    {
        "name": "Steenee",
        "id": 762
    },
    {
        "name": "Tsareena",
        "id": 763
    },
    {
        "name": "Comfey",
        "id": 764
    },
    {
        "name": "Oranguru",
        "id": 765
    },
    {
        "name": "Passimian",
        "id": 766
    },
    {
        "name": "Wimpod",
        "id": 767
    },
    {
        "name": "Golisopod",
        "id": 768
    },
    {
        "name": "Sandygast",
        "id": 769
    },
    {
        "name": "Palossand",
        "id": 770
    },
    {
        "name": "Pyukumuku",
        "id": 771
    },
    {
        "name": "Type: Null",
        "id": 772
    },
    {
        "name": "Silvally",
        "id": 773
    },
    {
        "name": "Minior",
        "id": 774
    },
    {
        "name": "Komala",
        "id": 775
    },
    {
        "name": "Turtonator",
        "id": 776
    },
    {
        "name": "Togedemaru",
        "id": 777
    },
    {
        "name": "Mimikyu",
        "id": 778
    },
    {
        "name": "Bruxish",
        "id": 779
    },
    {
        "name": "Drampa",
        "id": 780
    },
    {
        "name": "Dhelmise",
        "id": 781
    },
    {
        "name": "Jangmo-o",
        "id": 782
    },
    {
        "name": "Hakamo-o",
        "id": 783
    },
    {
        "name": "Kommo-o",
        "id": 784
    },
    {
        "name": "Tapu",
        "id": 785
    },
    {
        "name": "Tapu",
        "id": 786
    },
    {
        "name": "Tapu",
        "id": 787
    },
    {
        "name": "Tapu",
        "id": 788
    },
    {
        "name": "Cosmog",
        "id": 789
    },
    {
        "name": "Cosmoem",
        "id": 790
    },
    {
        "name": "Solgaleo",
        "id": 791
    },
    {
        "name": "Lunala",
        "id": 792
    },
    {
        "name": "Nihilego",
        "id": 793
    },
    {
        "name": "Buzzwole",
        "id": 794
    },
    {
        "name": "Pheromosa",
        "id": 795
    },
    {
        "name": "Xurkitree",
        "id": 796
    },
    {
        "name": "Celesteela",
        "id": 797
    },
    {
        "name": "Kartana",
        "id": 798
    },
    {
        "name": "Guzzlord",
        "id": 799
    },
    {
        "name": "Necrozma",
        "id": 800
    },
    {
        "name": "Magearna",
        "id": 801
    },
    {
        "name": "Marshadow",
        "id": 802
    },
    {
        "name": "Poipole",
        "id": 803
    },
    {
        "name": "Naganadel",
        "id": 804
    },
    {
        "name": "Stakataka",
        "id": 805
    },
    {
        "name": "Blacephalon",
        "id": 806
    },
    {
        "name": "Zeraora",
        "id": 807
    },
    {
        "name": "Meltan",
        "id": 808
    },
    {
        "name": "Melmetal",
        "id": 809
    }
]