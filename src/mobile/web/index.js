import React, { useEffect } from 'react';
import { BrowserRouter as Router, useLocation } from 'react-router-dom';
import Routes from './routes/index';
import { PersistGate } from 'redux-persist/es/integration/react';
import configureStore from './store/index';
import { Provider } from 'react-redux';
import Layout from '../components/layout/layout';
import 'semantic-ui-css/semantic.min.css';

const { persistor, store } = configureStore();

const Root = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <Router>
        <Layout>
          <Routes />
        </Layout>
      </Router>
    </PersistGate>
  </Provider>
);



export default Root;