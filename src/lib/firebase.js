import firebase from "firebase/app";
import "firebase/firestore";
// import "firebase/auth";
// import "firebase/firestore";
// import "firebase/storage";
// import "firebase/functions";

const Config = {
  apiKey: "AIzaSyDsOtcabT3FuGg_yR0A_J7ZxqwmJOBASBE",
  authDomain: "pokemon-tcg-demo.firebaseapp.com",
  projectId: "pokemon-tcg-demo",
  storageBucket: "pokemon-tcg-demo.appspot.com",
  messagingSenderId: "422630211800",
  appId: "1:422630211800:web:9aafb71ec3e68bfc9a17ad",
  measurementId: "G-EJSJ7TF2T8"
}

// let firebaseInitialized = false;
firebase.initializeApp(Config);
// firebaseInitialized = true;

const db = firebase.firestore();
// export const Firestore = firebaseInitialized ? firebase.firestore() : null;
// export const Firebase = firebaseInitialized ? firebase : null;
// export const FirebaseStorage = firebaseInitialized ? firebase.default.storage() : null;
// export const FirebaseAuth = firebaseInitialized ? firebase.default.auth() : null;
// export const FirebaseFunctions = firebase.default.functions();

export const LoadFirebase = () => {
  console.log('Firebase initialised.');
}

export const FIRESTORE = db;
