import { GENERATIONS } from './constants';

export const fetchPokemons = async (generation, page) => {
  const gen = GENERATIONS[generation];
  const URL = `https://api.pokemontcg.io/v2/cards`;
  return await fetch(`${URL}?q=nationalPokedexNumbers:${gen}&orderBy=nationalPokedexNumbers&page=${page}`, {
    headers: {
      'X-Api-Key': '65cf8216-21e5-416b-9a8d-e7cdd3247d5c'
    },
  }).then(res => res.json()).catch(err => {
    console.log(err);
  })
}

export const searchPokemons = async (number) => {
  const URL = `https://api.pokemontcg.io/v2/cards`;
  return await fetch(`${URL}?q=nationalPokedexNumbers:${number}`, {
    headers: {
      'X-Api-Key': '65cf8216-21e5-416b-9a8d-e7cdd3247d5c'
    },
  }).then(res => res.json()).catch(err => {
    console.log(err);
  })
}