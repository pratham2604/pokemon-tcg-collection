export const GENERATIONS = {
  gen_I: `[1 TO 151]`,
  gen_II: `[152 TO 251]`,
  gen_III: `[252 TO 386]`,
  gen_IV: `[387 TO 493]`,
  gen_V: `[494 TO 649]`,
  gen_VI: `[650 TO 721]`,
  gen_VII: `[722 TO 809]`,
  gen_VIII: `[810 TO 905]`,
  all: `all`,
}