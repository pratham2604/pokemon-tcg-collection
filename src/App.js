import './App.css';
import { useState } from 'react';
import { searchPokemons } from './lib/fetchPokemon';
import { GENERATIONS } from './lib/constants';
import { Modal, Image, Button, Grid, Header, Menu, Segment, Input } from 'semantic-ui-react';
import POKEMONS from './data/pokemons';
import {isMobile} from 'react-device-detect';
import MobileVersion from './../src/mobile';

function App() {
  const [currentTab, updateTab] = useState(0);
  const [searchString, updateString] = useState("");
  const headers = Object.keys(GENERATIONS);
  const switchTab = (index) => {
    updateTab(index);
    updateString("");
  }

  let debounce;
  const onSearch = (event) => {
    const { value } = event.target;
    clearTimeout(debounce)
    debounce = setTimeout(() => {
      updateString(value);
    }, 500);
  }
  let list = searchString ? POKEMONS.all.filter(pokemon => pokemon.name.toLowerCase().includes(searchString.toLowerCase()) ) : POKEMONS[headers[currentTab]];

  return (
    <>
      {isMobile ?
        <MobileVersion /> :
        <div className="App">
          <Menu pointing>
            {headers.map((title, index) => (
              <Menu.Item
                name={title}
                active={currentTab === index && !searchString}
                onClick={() => switchTab(index)}
                key={index} />
            ))}
            <Menu.Menu position='right'>
              <Menu.Item>
                <Input icon='search' placeholder='Search...' onChange={onSearch}/>
              </Menu.Item>
            </Menu.Menu>
          </Menu>
          <Segment className='data-wrapper'>
            {list.map((item) => <RowComponent key={item.id} item={item}/>)}
          </Segment>
        </div>
      }
    </>
  );
}

function RowComponent({ item }) {
  const { name, id } = item;
  const [showModal, toggleModal] = useState(false);
  const [showRows, updateShowRows] = useState(false);
  const [images, updateImages] = useState([]);
  const [data, updateData] = useState([]);
  const [loading, updateLoading] = useState(false);
  const [fetching, updateFetching] = useState(false);
  const showPictureModal = () => {
    if (data.length === 0) {
      updateLoading(true);
      searchPokemons(id).then(response => {
        const { data } = response;
        const items = data.map(item => item.images.large);
        updateImages(items)
        updateData(data);
        updateLoading(false);
        toggleModal(true);
      })
    } else {
      const items = data.map(item => item.images.large);
      updateImages(items)
      toggleModal(true);
    }
  }

  const fetchPokemonData = () => {
    if (data.length === 0) {
      updateFetching(true);
      searchPokemons(id).then(response => {
        const { data } = response;
        updateData(data);
        updateFetching(false);
        updateShowRows(true);
      })
    } else {
      updateShowRows(true);
    }
  }

  const showSingleCard = (images) => {
    updateImages(images)
    toggleModal(true);
  }

  const hideModal = () => {
    toggleModal(false);
    updateImages([]);
    // toggleModal(true);
  }
  return (
    <div className="data-row-container">
      {showRows ?
        <div>
          <div style={{display: 'flex'}}>
            <Button circular icon="minus" style={{cursor: 'pointer'}} onClick={() => updateShowRows(false)}></Button>
            <div className='card-name'><Header as="h3">{`#${id}  ${name}`}</Header></div>
            <Button onClick={showPictureModal}>Show Cards</Button>
          </div>
          <div className='card-container'>
            {data.map(card => {
              const { id, name, images } = card;
              return (
                <div className='card-row' key={id}>
                  <div style={{display: 'flex', flex: 1}}>
                    <Header as="h4">{name}</Header>
                  </div>
                  <Button onClick={() => showSingleCard([images.large])}>Show Card</Button>
                </div>
              )
            })}
          </div>
        </div> :
        <div style={{display: 'flex'}}>
          <Button loading={fetching} circular icon="plus" style={{cursor: 'pointer'}} onClick={fetchPokemonData}></Button>
          <div className='card-name'><Header as="h3">{`#${id}  ${name}`}</Header></div>
          <Button loading={loading} onClick={showPictureModal}>Show All Cards</Button>
        </div>
      }
      {showModal && <ImageModal name={name} onClose={hideModal} links={images}/>}
    </div>
  )
}

function ImageModal ({ onClose, name, links }) {
  return (
    <Modal
      onClose={onClose}
      open={true}
    >
      <Modal.Header>{name}</Modal.Header>
      <Modal.Content image>
        <Grid stackable>
          <Grid.Row>
            {links.map((link, index) => 
              <Grid.Column width={links.length > 1 ? 8 : 16} key={index}>
                <Image size='medium' src={link} style={{padding: '0.5rem'}} />
              </Grid.Column>
            )}
            
          </Grid.Row>
        </Grid>
        
      </Modal.Content>
    </Modal>
  )
}

export default App;
